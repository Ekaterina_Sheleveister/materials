//Д.З. Установить СУБД MySQL
//Создать таблицы user, group, user_group в БД

//cd C:\Program Files\MySQL\MySQL Server 5.7\bin
//cd C:\Program Files\MySQL\MySQL Server 5.7\bin\mysql 

//pert
//root 

//mysql -u petr -p
//123

//mysql -u frank -p
//123

//show databases;
//use my_db;
//show tables;

//select * from grp;

//create table ttt ( id int, name text);
//show tables;
//drop table ttt;

//create table user ( id int, name text);
//create table grp ( id int, name text);
//create table user_group ( id int, name text);



//DDL- язык для создания объектов базы данных
//Команды:
CREATE DATABASE — создать базу данных.
CREATE USER — создать пользователя.
CREATE TABLE — создать таблицу.
ALTER TABLE — модифицировать таблицу.
RENAME TO — переименовать таблицу.
CHANGE COLUMN — изменить имя и тип данных столбца.
MODIFY COLUMN — изменить тип данных или позицию столбца.
ADD COLUMN — добавить столбец в таблицу.
DROP COLUMN — удалить столбец из таблицы.
DROP TABLE — удалить таблицу

//DML - язык манипулирования данными
//Комманды:
SELECT — извлечение данных.
UPDATE — модификация данных. 
DELETE — удаление данных. 
INSERT INTO — вставка новых данных в таблицу

//Типы данных
INT — целое число. (INTEGER) 
FLOAT — число с плавающей точкой.
VARCHAR — текстовые данные длиной до 255 . 
CHAR — набор символов фиксированной длины.
TEXT — набор с максимальной длиной 65535 .
DATE — дата.
DATETIME — дата и время.
BLOB — массив двоичных данных

//работа с консолью 
shell> mysql -u user_name -p -h
host_name db_name

//Просмотр существующих БД
mysql> show databases;

//Выбор БД для работы
mysql> use databases_name;

//Создание новой базы данных
mysql> CREATE DATABASE my_db CHARACTER
SET utf8 COLLATE utf8_general_ci;

mysql> CREATE DATABASE stockfinder2 CHARACTER
SET utf8 COLLATE utf8_general_ci;

//Удаление БД
mysql> DROP DATABASE IF EXISTS my_db;

//Создание пользователя
mysql> CREATE USER 'frank'@'localhost' 
IDENTIFIED BY '123';

//Создание пользователя
mysql> CREATE USER 'petr'@'localhost'
IDENTIFIED BY '123';

//Удаление пользователя
mysql> DROP USER 'ivan'@'localhost'

//Выдача привилегий на БД
GRANT ALL PRIVILEGES ON my_db.* TO
'frank'@'localhost'

//Выдача привилегий на БД
GRANT ALL PRIVILEGES ON my_db.* TO
'petr'@'localhost';

//Выдача привилегий на БД
GRANT ALL PRIVILEGES ON stockfinder.* TO
'petr'@'localhost';

//Вход под новым пользователем
shell> mysql -u ivan -p my_db
//Enter password> 123

//Загрузка скриптов
mysql> source C:\scripts\my_commands.sql;
mysql> source ..\scripts\my_commands.sql;

//Другой способ загрузки скриптов
shell> mysql db_name < input_file

//Проверка локали подключения
mysql> status;

//Использование помощи
mysql> help contents;

//Просмотр структуры таблицы
mysql> show tables;
mysql> desc table_name;

//Однотабличные запросы
SELECT col1,col2,col3
FROM table_name
WHERE expression

//Пример без указания таблицы
SELECT 2+2, REPEAT('x',5),
DATE_ADD('2001-01-01',INTERVAL 7 DAY),
1/0;

//Выборка из таблицы
SELECT ID, name, countrycode, pulation
FROM city;
SELECT * FROM City; //выбрать все столбцы

//Арифметика в столбцах
SELECT id*200, name, countrycode + 10
FROM city;

//Синонимы для столбцов
select 2+3 sign, countrycode cc
from city;
// используем как внятное название для
//формул или сокращение для больших имен

//Предикат уловия where
SELECT * FROM city where id = 123 ;

//конструкция для набора IN
SELECT * FROM city where id in (123,7,5);

//выбор по шаблону LIKE
SELECT * FROM city
WHERE name like '%Petersburg%';

//шаблон создания таблицы
CREATE TABLE table_name
(
id_table_name INT NOT NULL AUTO_INCREMENT,
login varchar(30) default NULL,
password varchar(20) default NULL,
PRIMARY KEY (id_table_name)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8 ;


//ALTER TABLE - если забыли задать ключ
ALTER TABLE table_name ADD COLUMN
id_table_name INT NOT NULL
AUTO_INCREMENT FIRST, ADD PRIMARY KEY
(id_table_name);


//Как добавить связь между таблицами
ALTER TABLE child_name
ADD CONSTRAINT name_constraint
FOREIGN KEY(id_parent)
REFERENCES parent_name(id_parent)
ON DELETE RESTRICT

//Пример добавления связей между таблицами
ALTER TABLE child ADD CONSTRAINT cnst_child_ref_parent
FOREIGN KEY (id_parent)
REFERENCES parent(id_parent)
ON DELETE RESTRICT


//Как удалить таблицу ?
DROP TABLE table_name;


//INSERT — вставка записей
1) INSERT INTO table_name (id_table_name, login,
passwd) VALUES (NULL, 'admin', '123456');

2) INSERT INTO table_name (id_table_name, login,
passwd) VALUES (1, 'admin', '123456');

3) INSERT INTO table_name
VALUES ('', 'admin', '123456');

4) INSERT INTO table_name (login, passwd)
VALUES ('admin', '123456');

5) INSERT INTO table_name (id_table_name, last_name,
first_name) VALUES (99, 'admin', '123456');

//DELETE- удаление записей
DELETE FROM table_name
WHERE id_table_name = 99


//example Ruslan
CREATE TABLE user ( 
id_user INT NOT NULL AUTO_INCREMENT, 
login varchar(30) default NULL, 
password varchar(20) default NULL, 
passwordRepeat varchar(20) default NULL, 
FullName varchar(20) default NULL, 
fullage varchar(20) default NULL, 
email varchar(20) default NULL, 
phoneNumber varchar(20) default NULL, 
postalAddress varchar(20) default NULL, 
genderRadios varchar(20) default NULL, 
confirmLicense varchar(20) default NULL, 
PRIMARY KEY (id_user)) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 ; 

//start my example
//создание таблицы USER
CREATE TABLE USER 
(
id_user INT NOT NULL AUTO_INCREMENT,
FirstName varchar(30) default NULL,
LastName varchar(20) default NULL,
DayBirthday varchar(20) default NULL,
Adress varchar(20) default NULL,
Age varchar(20) default NULL,
Login varchar(20) default NULL,
Password varchar(20) default NULL,
Activity varchar(20) default NULL,
PRIMARY KEY (id_user)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

//создание таблицы USERS - для добавления из input
CREATE TABLE USERS 
(
userId INT NOT NULL AUTO_INCREMENT,
userName varchar(30) default NULL,
PRIMARY KEY (userId)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

//создание таблицы EVENTS - для добавления из input
//eventsId
//eventsGroup
//dataStart
//dataEnd
//lesson
//colorField
CREATE TABLE EVENTS 
(
eventsId INT NOT NULL AUTO_INCREMENT,
eventsGroup varchar(30) default NULL,
dataStart varchar(30) default NULL,
dataEnd varchar(30) default NULL,
lesson varchar(30) default NULL,
colorField varchar(30) default NULL,
PRIMARY KEY (eventsId)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE customer 
(
id INT NOT NULL AUTO_INCREMENT,
name varchar(30) default NULL,
address varchar(30) default NULL,
email varchar(30) default NULL,
phone varchar(30) default NULL,
PRIMARY KEY (id)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO events (eventsId, eventsGroup, dataStart, dataEnd, lesson, colorField) 
VALUES (NULL, NULL, NULL, NULL, NULL, NULL);

//создание таблицы GRP
CREATE TABLE GRP
(
id_grp INT NOT NULL AUTO_INCREMENT,
Name varchar(20) default NULL,
Rules varchar(20) default NULL,
PRIMARY KEY (id_grp)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

//создание таблицы USER_GRP
CREATE TABLE USER_GRP
(
id_user_grp INT NOT NULL AUTO_INCREMENT,
id_user INT default NULL,
id_grp INT default NULL,
PRIMARY KEY (id_user_grp)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

//создание связей 
ALTER TABLE user_grp 
ADD CONSTRAINT cnst_userGrp_ref_user
FOREIGN KEY(id_user)
REFERENCES user(id_user)
ON DELETE RESTRICT;

ALTER TABLE user_grp 
ADD CONSTRAINT cnst_userGrp_ref_grp
FOREIGN KEY(id_grp)
REFERENCES grp(id_grp)
ON DELETE RESTRICT;

//вставка записей
INSERT INTO user (id_user, login, password) 
VALUES (NULL, 'admin', '123456');

INSERT INTO user (id_user, FirstName, LastName) 
VALUES (NULL, 'Vinsent', 'Vangog');

INSERT INTO user (id_user, FirstName, LastName) 
VALUES (NULL, 'Gustav', 'Climt');

INSERT INTO users (userId, userName) 
VALUES (NULL, 'Gustav');

FirstName varchar(30) default NULL,
LastName varchar(20) default NULL,
DayBirthday varchar(20) default NULL,
Adress varchar(20) default NULL,
Age varchar(20) default NULL,
Login varchar(20) default NULL,
Password varchar(20) default NULL,
Activity varchar(20) default NULL,

INSERT INTO user (id_user, FirstName, LastName, DayBirthday, Adress, Age, Login, Password, Activity) 
VALUES (NULL, 'Gustav', 'Climt', '20.01.1987', 'Nevsky st.', '26', 'Gustav', '123', 'true');

INSERT INTO user (id_user, FirstName, LastName, DayBirthday, Adress, Age, Login, Password, Activity) 
VALUES (NULL, 'Leonardo', 'Davinchi', '15.01.1987', 'Nevsky st.', '28', 'Leonardo', '123', 'true');

INSERT INTO user (id_user, FirstName, LastName, DayBirthday, Adress, Age, Login, Password, Activity) 
VALUES (NULL, 'Tulus', 'Lotrek', '15.03.1985', 'Nevsky st.', '30', 'Tulus', '123', 'true');

INSERT INTO user (id_user, FirstName, LastName, DayBirthday, Adress, Age, Login, Password, Activity) 
VALUES (NULL, 'Vinsent', 'Vangog', '15.03.1963', 'Nevsky st.', '102', 'Vinsent', '123', true);

INSERT INTO user (id_user, FirstName, LastName, DayBirthday, Adress, Age, Login, Password, Activity) 
VALUES (NULL, 'Ayn', 'Rend', '15.03.1963', 'Nevsky st.', '87', 'Ayn', '123', true);

INSERT INTO user (id_user, FirstName, LastName, DayBirthday, Adress, Age, Login, Password, Activity) 
VALUES (NULL, 'Jhorj', 'Martin', '15.03.1994', 'Nevsky st.', '24', 'Jhorj', '123', true);

INSERT INTO user (id_user, FirstName, LastName, DayBirthday, Adress, Age, Login, Password, Activity) 
VALUES (NULL, 'Ludvig', 'Bethoven', '15.03.1963', 'Nevsky st.', '56', 'Vinsent', '123', true);

id_grp INT NOT NULL AUTO_INCREMENT,
Name varchar(20) default NULL,
Rules varchar(20) default NULL,

INSERT INTO grp (id_grp, Name, Rules) 
VALUES (NULL, 'Admin', 'Root');

INSERT INTO grp (id_grp, Name, Rules) 
VALUES (NULL, 'Director', 'VipRoot');

INSERT INTO grp (id_grp, Name, Rules) 
VALUES (NULL, 'Manager', 'Limited');

INSERT INTO grp (id_grp, Name, Rules) 
VALUES (NULL, 'Stager', 'Block');

id_user_grp INT NOT NULL AUTO_INCREMENT,
id_user INT default NULL,
id_grp INT default NULL,

INSERT INTO user_grp (id_user_grp, id_user, id_grp) 
VALUES (NULL, 7, 1);

INSERT INTO user_grp (id_user_grp, id_user, id_grp) 
VALUES (NULL, 7, 2);
//end my example


CREATE TABLE grp ( 
id_grp INT NOT NULL AUTO_INCREMENT, 
name varchar(30) default NULL, 
rules varchar(20) default NULL, 
PRIMARY KEY (id_grp)) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 ; 

CREATE TABLE user_grp ( 
id_user_grp INT NOT NULL AUTO_INCREMENT, 
id_grp INT default NULL, 
id_user INT default NULL, 
PRIMARY KEY (id_user_grp)) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 ; 

ALTER TABLE user_grp 
ADD CONSTRAINT cnst_userGrp_ref_user 
FOREIGN KEY(id_user) 
REFERENCES user(id_user) 
ON DELETE RESTRICT; 

ALTER TABLE user_grp 
ADD CONSTRAINT cnst_userGrp_ref_grp 
FOREIGN KEY(id_grp) 
REFERENCES grp(id_grp) 
ON DELETE RESTRICT;

//Агрегатные функции
1) SUM() - суммирует значения столбца
//шаблон запроса 
SELECT col1, summ(amount)
FROM table_name
WHERE expression
GROUP BY col1
HAVING summ(col1) > value
ORDER BY col2

// Получим сумму заказа по корзине
//basket, goods, basket_goods 
SELECT col1, summ(bg.amount*g.price)
FROM basket_goods bg, goods g
WHERE bg.id_goods = g.id_goods
AND id_basket = 1
GROUP BY id_basket

// Получим корзины где сумма заказа > 1000
SELECT bg.id_basket,summ(bg.amount*g.price)
FROM basket_goods bg, goods g
WHERE bg.id_goods = g.id_goods
GROUP BY bg.id_basket
HAVING summ(bg.amount*g.price) > 1000
ORDER BY bg.id_basket

2) AVG() - среднее значение в столбце

3) MIN() — наименьшее значение в столбце.
// - Минимальное кол во купленного товара
уникальных товаров в корзине
select min(amount) from basket_goods;

4) MAX() — наибольшее значение в столбце.
// - Максимальное кол во купленного товара
select max(amount) from basket_goods;

5) COUNT() — количество записей в столбце.
// - Кол во товарных позиций на складе
select count(*) from goods;
// - Кол во уникальных товаров в корзине
select count(distinct id_goods) from
basket_goods;

6) DISTINCT — выводит значения без повторов.

//Сколько групп
//решение 4 
select count(*) from grp;

//Сколько пользователей от 25 до 40 в таблице user 
SELECT col5, summ(amount)
FROM user
WHERE expression
GROUP BY col1
HAVING summ(col1) > 25 & < 40
ORDER BY col5

// Получим корзины где сумма заказа > 1000
SELECT count(distinct )
FROM basket_goods bg, goods g
WHERE bg.id_goods = g.id_goods
GROUP BY bg.id_basket
HAVING summ(bg.amount*g.price) > 1000
ORDER BY bg.id_basket

select count, max(40)
from user 

//Проверка user в group. Ищем пользователей, которые без группы. Select 
SELECT a.name, b.name
FROM goods a
left join characters b
on a.id_goods = b.id_goods
WHERE b.name IS NULL ;

//решение первой задачи
select a.id_user, b.id_user
from user a 
left join user_grp b
on a.id_user = b.id_user
where b.id_user_grp is null ;

//
select a.name , b.value
from table1 a, table2 b
where a.id = b.id 

//решение2 
select a.id_user, b.id_grp
from user a, user_grp b
where a.id_user = b.id_user
and a.id_user = 1

//удаление данных из таблицы
DELETE [LOW_PRIORITY | QUICK] FROM table_name
       [WHERE where_definition]
       [ORDER BY ...]
       [LIMIT rows]
	   

DELETE FROM events WHERE 'id' = 71;
DELETE FROM events WHERE 1 = 1;

//create db in json
CREATE TABLE `products` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`name` JSON,
`specs` JSON,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO products VALUES(
    null,
    '{"en": "phone", "it": "telefono"}',
    '{"colors": ["black", "white", "gold"], "size": {"weight": 1, "height": 1}}'
);

INSERT INTO products VALUES(
    null,
    '{"en": "screen", "it": "schermo"}',
    '{"colors": ["black", "silver"], "size": {"weight": 2, "height": 3}}'
);

INSERT INTO products VALUES(
    null,
    '{"en": "car", "it": "auto"}',
    '{"colors": ["red", "blue"], "size": {"weight": 40, "height": 34}}'
);

//Считывание значений JSON
select
name->"$.en" as name,
specs->"$.size.weight" as weight,
specs->"$.colors" as colors
from products;

name        weight      colors
'phone'	    1	        ['black', 'white', 'gold']
'screen'    2	        ['black', 'silver']
'car'	    40	        ['red', 'blue']

json_decode( Products::selectRaw('name->"$.en" as name')->first()->name )
