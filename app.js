var express         = require('express');
var path            = require('path');
var favicon         = require('serve-favicon');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var sys             = require('util');
var mustache        = require('mustache');
var mustacheExpress = require('mustache-express');
var http            = require('http');
var multer          = require('multer');
var fs              = require('fs');
var connect         = require('connect');
var router          = express.Router();

var customersNode   = require('./app_server/controllers/customersNode');
var customersJava   = require('./app_server/controllers/customersJava');
var customersPython = require('./app_server/controllers/customersPython');

var nodejs = require('./app_server/routes/nodejs');
var python = require('./app_server/routes/python');
var java = require('./app_server/routes/java');


var app = express();
var port = process.env.PORT || 4000;

app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', path.join(__dirname, 'app_server', 'views'));


var mysql      = require('mysql');
var connection = mysql.createConnection({
    host       : 'localhost',
    user       : 'petr',
    password   : 'root',
    database   : 'users_nodejs'
});


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/nodejs', nodejs);
app.use('/java', java);
app.use('/python', python);



module.exports = app;
app.listen(port);
console.log('Server listening ' + port);