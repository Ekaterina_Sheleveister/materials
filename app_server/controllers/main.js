var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var http = require('http');
var multer = require('multer');
//var upload = multer({ dest: 'uploads/' });

var storage = multer.diskStorage({
    destination: 'uploads/', 
    filename: function (req, file, cb) {
        cb(null, file.originalname.replace(' ', '_'));
  }
})

var upload = multer({ storage: storage })

/*
//template with simple object
exports.list = function(req, res) {
    
    var data = '[{"id": "1", "course": "NodeJS", "name": "ER-моделирование", "material": "2_tamplate.pdf", "tasks": "index.js" }, {"id": "2", "course": "NodeJS", "name": "Архитектура клиент-сервер", "material": "3_exercise_3_object.pdf", "tasks": "app.js"}, {"id": "3", "course": "NodeJS", "name": "Шаблонизация", "material": "4_exercise_4_npm_express.pdf", "tasks": "server.js"}, {"id": "4", "course": "NodeJS", "name": "Архитектура клиент-сервер", "material": "3_exercise_3_object.pdf", "tasks": "app.js"}, {"id": "5", "course": "NodeJS", "name": "Шаблонизация", "material": "4_exercise_4_npm_express.pdf", "tasks": "server.js"}]';
    var data = JSON.parse(data);
    
    //res.render('nodejs', { title: "Мaтериалы для курса Node.js" } );  
    res.render('nodejs', { title: "Мaтериалы для курса Node.js", data: data } ); 
    
};
*/

//exports.list = function(req, res) {
//  res.render('users', { title: 'Hello World' });
//};

//рендер таблицы из бд mysql
exports.list = function(req, res) {
    req.getConnection(function(err, connection) {
        var query = connection.query('SELECT * FROM materials', function(err, rows) {
            if(err)
            console.log("Error Selecting : %s ", err );
            res.render('java', { page_title: "Customers - Node.js", data: rows });
            console.log(rows);
            console.log(rows[0].id); //2
            console.log(rows[0].name); //Nadya Eka
            console.log(rows[0].material); //Nadya Eka
            /*
            [ RowDataPacket { id: 2, name: 'Nadya Eka' },
              RowDataPacket { id: 3, name: 'Amali' },
              RowDataPacket { id: 4, name: 'Angel ' },
              RowDataPacket { id: 5, name: 'Ujang' } ]
            */
        });
        //console.log(query.sql);
    });
};

//добавить в бд из формы и отрендерить на страницу 
exports.save = function(req,res) {
    var input = JSON.parse(JSON.stringify(req.body));
    //var id = req.params.id;
    req.getConnection(function (err, connection) {
        var data = {
            course   : "Nodejs", 
            name     : input.name, 
            material : req.files[0].originalname, 
            tasks    : req.files[0].originalname 
        };
        var query = connection.query( "INSERT INTO materials set ? ", data, function(err, rows) {
            if (err)
            console.log("Error inserting : %s ", err );
            //res.redirect('/users');
        });
        
        
        console.log(req.files); //json result
        console.log(req.files[0].fieldname); //json result
        console.log(req.files[0].originalname); //json result
        
        var path = require('path'); // get path
        var dir = path.resolve(".")+'/uploads/'; // give path
        
        var query = connection.query('SELECT * FROM materials', function(err, rows) {
            if(err)
            console.log("Error Selecting : %s ", err );
            res.render('index', { message: "Файл добавлен", data: rows });
        }); 
                
    });
};

//удалить со страницы и из бд
exports.delete_customer = function(req,res) {   
    var id = req.params.id;
    req.getConnection(function (err, connection) {
        connection.query("DELETE FROM materials  WHERE id = ? ", [id], function(err, rows) {
            if(err)
            console.log("Error deleting : %s ", err );
            res.redirect('/user-materials');
        });
    });
};

//exports.upload = function(req, res, next) {
//    //res.send(req.files); //json result
//    console.log(req.files); //json result
//    console.log(req.files[0].fieldname); //json result
//    console.log(req.files[0].originalname); //json result
//    
//    res.render('index', { title: 'Photo upload page', message: "Successfully uploaded your file"});
//}

//по ajax рендерим на страницу файлы из папки uploads
exports.download = function(req, res) { // create download route
    var path = require('path'); // get path
    var dir = path.resolve(".")+'/uploads/'; // give path
    fs.readdir(dir, function(err, list) { // read directory return  error or list
        if (err) return res.json(err);
        else
        res.json(list);
    });
};

//скачать файл в папку на комп 
exports.file = function(req, res, next) { // this routes all types of file
    var path = require('path');
    var file = req.params.file;
    var path = path.resolve(".")+'/uploads/'+file;
    res.download(path); // magic of download fuction
};