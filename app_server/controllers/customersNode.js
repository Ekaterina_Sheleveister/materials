var express    = require('express');
var router     = express.Router();
var fs         = require('fs');
var path       = require('path');
var http       = require('http');
var multer     = require('multer');
var app        = express();
var mysql      = require('mysql');

var connection = mysql.createConnection({
    host       : 'localhost',
    user       : 'petr',
    password   : 'root',
    database   : 'users_nodejs'
});

connection.connect(function(err) {
    if(!err) {
        console.log("Database Node.js is connected");    
    } else {
        console.log("Error connecting database Node.js");    
    }
});

//рендерим json из бд по id = 1, который соответствует курсу node.js
exports.list = function(req, res) {
    //res.render('nodejs', { title: "Мaтериалы для курса Node.js"});
    connection.query('SELECT data FROM materials_courses WHERE id = 1', function(err, rows) {
        
        if(err) {
            console.log("Error Selecting : %s ", err );
        } else {
            var data = JSON.parse(rows[0].data);
            var dataId = data.length;
            res.render('materials', { title:       "Мaтериалы для курса Node.js", 
                                      image:       "img/nodejs.jpg", 
                                      action:      "/nodejs",
                                      href:        "../nodejs/",
                                      global_path: "js/global_node.js",
                                      data:        data });
            //console.log("data_list", (data[1]));
            //console.log(dataId, typeof dataId);
        }
    });    
};

//добавить в бд из формы и отрендерить на страницу 
//добавляет файл в папку uploads
//первое число в имени файла должно соответствовать номеру занятия 
//придется переименовывать файл корректно перед загрузкой
exports.save = function(req, res, next) {
    var input = JSON.parse(JSON.stringify(req.body));
    
//    console.log(req.files);                 // {}
//    console.log(req.files[0].fielname);    //'1_lesson_2_http.pdf',
//    console.log(req.files[0].fielname); //'1 lesson_2_http.pdf',
    

    connection.query('SELECT data FROM materials_courses WHERE id = 1', function(err, rows) {
        var data = JSON.parse(rows[0].data);
        var dataId = data.length;
        //console.log("data", data);
        //console.log("return dataId", dataId);
        
        /* ========================================================= 
        todo: добавить проверку загрузки, если файл не загружен -> 
        свойство req.files[0].filename не прочитать, тк оно отсутствует в multer 
           ========================================================= */
        var objMaterials = {
            id: dataId + 1,
            course: "NodeJS",
            name: input.name,
            material: req.files[0].filename, // название дает модуль multer, из массива upload
            tasks: req.files[1].filename     // читает из массива второй загруженный объект
        };
        //console.log(objMaterials);
        
        data.push(objMaterials);
        var updated_data = JSON.stringify(data);
        //console.log("updated_data", updated_data);

        connection.query( "UPDATE materials_courses SET `data` = ? Where ID = 1", [updated_data], function(err, rows) {
            if (err) console.log("Error inserting : %s ", err );
        });

        var path = require('path'); // get path
        var dir = path.resolve(".")+'/uploads/nodejs/'; // куда сохраняет
        
        //res.redirect('/nodejs');

        connection.query('SELECT data FROM materials_courses WHERE id = 1', function(err, rows) {
            if(err) console.log("Error Selecting : %s ", err );
            var data = JSON.parse(rows[0].data);
            res.render('materials', { message:     "Файл добавлен", 
                                      title:       "Мaтериалы для курса Node.js", 
                                      image:       "img/nodejs.jpg", 
                                      action:      "/nodejs",
                                      href:        "../nodejs/",
                                      global_path: "js/global_node.js",
                                      data:        data });
        });          
    });    
};

//удаление со страницы и из базы данных 
exports.delete_customer = function(req, res) {   
    
    var id = req.params.id;
    console.log("id", id);
    var dataId = id - 1;
     
    connection.query('SELECT data FROM materials_courses WHERE id = 1', function(err, rows) {
        var data = JSON.parse(rows[0].data); //получаем объект
        //console.log("data_delete", data);      // all array 
        //console.log("data_id", data[dataId]);  // {}
        //console.log("id", id, typeof id);      // id 1 string  
        
        data.splice(dataId, 1);
        
        //после удаления восстанавливаем порядок нумерации занятий в соответствии с порядковым номером в массиве
        for (let i = 0; i < data.length; i++) {
            //console.log(+(data[i]).id);
            (data[i]).id = String( i + 1 );
        }
        //console.log(data);
        
        var deleted_data = JSON.stringify(data); //превращаем в json 
        connection.query( "UPDATE materials_courses SET `data` = ? Where ID = 1", [deleted_data], function(err, rows) {
            if (err) console.log("Error inserting : %s ", err );
        });
        
        res.redirect('/nodejs');

    });  
};

//функция отображения файлов с материалами из папки upload
//файлы на странице отображаются через ajax-запрос 
exports.download = function(req, res) { 
    
    var path = require('path'); 
    var dir = path.resolve(".")+'/uploads/nodejs/'; 
    
    fs.readdir(dir, function(err, list) { 
        if (err) {
            return res.json(err);    
        } else {
            res.json(list);
        }
    });
};

//скачать файл со страницы в папку на комп
exports.file = function(req, res, next) { 
    
    var path = require('path');
    var file = req.params.file;
    var path = path.resolve(".")+'/uploads/nodejs/' + file; // путь откуда берет файл для скачивания
    
    res.download(path); 
    
    console.log("file", file); // node.pdf
    console.log("path2", path); // D:\PROJECTS\NODE-JS\test\itmonet_template/uploads/nodejs/node.pdf
    
};